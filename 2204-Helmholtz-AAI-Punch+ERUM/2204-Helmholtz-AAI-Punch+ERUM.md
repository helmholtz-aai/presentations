---
# vim:tw=100:ft=markdown
author: <p align=left><white><small>Marcus Hardt (on behalf of the HIFIS AAI Task)</small></white></p>
title: <p align=left><white><br/><br/><small> Helmholtz-AAI </small></white></p>
date: <p align=left><white>April 2022</white></p>
theme: marcus
parallaxBackgroundImage: images/helmholtz-bg-slide-new.png
title-slide-attributes:
    data-background-image: images/helmholtz-bg-head-new.png
slideNumber: \'c/t\'
preloadIframes: true
pdfSeparateFragments: false
pdfMaxPagesPerSlide: 1
showNotes: false
mouseWheel: true
transition: none
backgroundTransition: none
---

# General Part: **Architecture**

## Concept for modern AAIs
- EU Project AARC
    - Analyse **existing** Architectures
    - Analyse **existing** Policies
- Give recommendations 
- [AARC Blueprint Architectures](https://aarc-community.org/architecture/) 
    - Introduction of the "proxy" component
- [AARC Policy Development KIT](https://aarc-community.org/policies/policy-development-kit/)
    - Fundamental policy templates for
        - Operating an infrastructure
        - Handling Incident Response
        - Manage Members
        - Requirements on Authentication
        - Risk Assessment
        - Data Protection
        - Privacy Policy
        - Service Operation
        - Acceptable Use Policy
    - (Designed to be GDPR compliant)


## The **"proxy"** component
- Scalability!
    - Stop every IdP needing to talk to every SP (`2800 * 1800`)
    - Scalable Attribute release 
        - (Different nations, different IdPs, each with different schema)
- Third party **authorisation**
    - *"Community Attribute Services"*
- Enforcement of authorisation
- Protocol Translation Translation
    - SAML, OIDC, X.509
- Stackable


## {data-background-image="images/bpa-final-glow.png" data-background-size="contain"}


## Evolution of the BPA (~2019)
- Add differentiation:
    - Between **community** and **infrastructure**
    - Between "infrastructure run by a community" and "general e-Infrastructure"
    - Different types of Services
- Introduces the vocabulary used 
- Full Document: <small>[https://zenodo.org/record/3672785/files/AARC-G045-AARC_BPA_2019-Final.pdf](https://zenodo.org/record/3672785/files/AARC-G045-AARC_BPA_2019-Final.pdf)</small>


## {data-background-image="images/bpa2019.png" data-background-size="contain"}



# <small>One specific Community AAI:</small><br/> Helmholtz AAI

## Relation to DFN AAI

## {data-background-image="images/foederationen_und_bpa.png" data-background-size="contain"}


## Authentication
- Home IdP
    - Germany: DFN
    - Globe: EduGAIN
    - Others: Google, GitHub, Orcid, umbrellaID, ...
- Not yet 
    - Identity Linking
    - National e-ID


## Authorisation Management<br/>Based on **Community**
- **Virtual Organisation (VO)** approach
- Very similar: HPC compute projects
- **VO** Managers can administer community members
- Services can filter users by
    - VO Attributes
        - `climate -> ozone -> south-pole`
        - `cern -> cms -> admin`


## Authorisation Management<br/>Based on **Origin**
- **Home-IdP based** approach
- Home IdP can assert complementary information
- Services can filter users by
    - Home-Org asserted eligibility to use certain resources
    - Status: - Employee / Student / Guest


## Authorisation Management<br/>Based on **Assurance**
- Levels of Assurance: [REFEDS Assurance Framework](https://refeds.org/assurance)
    - Passport seen, Work-Contract available (Most academic Institutes)
    - Uniqueness of the identifier
    - Freshness of attributes
    - Verified Email Address (Social Media)
- Benefit
    - AAI can host "lesser-than-maximum" users
    - Scientists only need to upgrade their identity, if necessary to access service
    - Services can provide different levels of access


## <small>**Interfaces for Service Integration**</small>
- Translation between Multiple Protocols:
    - Identities: SAML, OpenID Connect, X.509
    - Services: OpenID Connect, SAML
- Examples for integrated services
  - Gitlab
  - RocketChat
  - OpenStack
  - Helmholtz Federated IT services (HIFIS, [https://www.hifis.net](https://www.hifis.net)) 
    - HIFIS Cloud [https://cloud.helmholtz.de](https://cloud.helmholtz.de/#/services)
    - Drives further development, documentation and service integration
- More docs at Helmholtz AAI:
  - [https://aai.helmholtz.de/doc](https://aai.helmholtz.de/doc)

## Adanced example:<br/> Keycloak Infrastructure proxy
- Use case:
    - DESY operates the dCache storage infrastructure
    - Local DESY users already possess data (Terabytes)
    - Goal: Allow external users, while not touching existing internal ones
- Solution
    - **Yet Another Proxy!** (known as "Infrastructure Proxy")
    - In this case: Keycloak
        - Handle incoming users
        - Check with local user-db
        - Forward user to services (dCache is just the first one)
- Result:
    - Authorisation delegated to VO Managers
    - Reuse existing infrastructure setup (no additional instance of dCache)
    - A single infrastructure proxy allows connection of legacy site services

## Helmholtz AAI: Numbers {
    data-background-image="images/aai_users-plot_2.png" data-background-size="contain"
    data-transition="none-in zoom-out"
    data-transition-speed="slow"
    data-background-opacity="0.1"
          }

## {data-background-image="images/aai_users-plot_2.png" data-background-size="contain"}


# Caution


## Design Constraints
- Infrastructure Proxies
    - Collect infrastructures
    - Provide them to Communities
- Cross community access is not trivial to support
    - Different identifiers issued by different community AAI
    - Key question: What is a good size for a community:
        - The Community of German Researchers
        - The community of Bavarian Alpine Yeti Researchers
 - <clean class="fragment"data-fragment-index="1">**Fragmentation should be avoided!!!**
<li class="fragment"data-fragment-index="2"> Identity-linking is not supported:
    - This is a major effort
    - Creates a lot of problems in the details
    - **Hell breaks loose, when deprovisioning linked accounts**
</li>



# Bottomline
## Summary
-  **Do not build your own AAI**
    - You are likely to repeat some of the stone, bronze, and/or iron-ages
    - You **will** increase the fragmentation of communities and resources
- Some communities are already organised
    - I.e. they run their own community AAI
    - Why not join forces with them?
-  Users always live in the context of their community AAI
    - Many AAIs will cause **fragmentation**
    - Difficult to cooperate across community borders



