Die Helmholtz AAI[1] hat das Ziel, einen einheitlichen Zugriff auf IT
Dienste innerhalb der Helmholtz Gemeinschaft zu ermöglichen. Dabei sollen
Kooperationspartner (Gäste) genau so wie Helmholtz-Mitarbeiter die
Identität ihrer jeweiligen Heimat Einrichtung verwenden. Die
Unterscheidung dient hierbei im wesentlichen der Rechtevergabe.
Beispielsweise können Helmholtz-Mitarbeiter HPC-Projekte, Nextcloud Shares
oder Gitlab Projekte anlegen, und Gästen Zugriff darauf geben. Die
Rechtevergabe erfolgt dabei sowohl auf der Mitgliedschaft in Virtuellen
Organisationen, als auch auf Basis von Eigenschaften innerhalb der
Heimatorganisation (z.B. Student, Gast, Mitarbeiter).

Um den Kreis an Nutzern möglichst groß zu gestalten werden zusätzlich zu
eduRoam Nutzern auch solche aus Social IdPs zugelassen (aktuell Orcid,
Google und Github).  Die Helmholtz AAI kennzeichnet die Nutzer über
Verlässlichkeitsmerkmale basierend auf dem REFEDS Assurance Framework [2]


Es werden SAML und OpenID COnnect (OIDC) Dienste unterstützt. Die
überwiegende Mehrheit der bereits integrierten Dienste setzt auf OpenID
Connect. Hierzu zählen von einfachen Web-Diensten wie Gitlab, Mattermost
und NextCloud auch komplexere Dienste, wie OpenStack, JupyterHub und der
Zugriff auf den GPU Cluster HAICore, der über die KIT RegAPP realisiert
wurde.

Helmholtz AAI und die angeschlossenen Dienste werden kontinuierlich
weiterentwickelt. So wird beispielsweise an der Integration von Systemen,
die standortübergreifend identische Nutzer bedienen über den Helmholtz
Cloud Agent, oder die vertefte Integration von Unix Diensten (z.B. ssh
oder sudo) mit föderierten Identitäten gearbeitet.

Das Weiterreichen von Deprovisionierungsinformationen ist implementiert
und ermöglicht Diensten, die dies unterstützen, eine Benachrichtigung zu
erhalten, wenn Nutzer aus einer Gruppe entfernt werden, oder die
Heimateinrichtung verlassen.


Alle Attribute und deren Werte halten sich an die Empfehlungen von AARC
Guidelines [3]. Das selbe gilt für die Organisation der
Verantwortlichkeiten und der Kommunikation aller Mitspieler in der AAI.
Diese orientiert sich am AARC Policy Development KIT, um mit den
vorgehensweisen der internationalen Partner kompatibel zu sein.


Die Architektur der Helmholtz-AAI folgt der AARC Blueprint Architektur,
dem so genannten Prox-Modell. Dabei werden die Attribute über einen Nutzer
von der Heimateinrichtung an den im Forschungszentrum Jülich (FZJ)
betriebenen Proxy übermittelt, der dann einen einheitlichen Satz an
Attributen (ergänzt um weitere Informatinen über den Nutzer) an die
Dienste weiterleitet. Dies geschieht nach ausdrücklicher Prüfung durch den
Datenschutzbeauftragten des FZJ.


Einen übergeordneten Identifikator, wie z.b. eine EDU-ID wird aktuell am
Beispiel von ORCID unterstützt. Diese ID können Nutzer aktuell manuell
eintragen. Lösungen die dies durch Automatisierung vereinfachen,
nutzerfreundlich und sicherer gestalten  können integriert werden.


Aktuell (Stand Ende 2021) können ca 6000 Nutzer auf zehn produktive
Dienste zugreifen. Wir verzeichnen einen steten Zuwachs von ca 1000
Nutzern im Monat.


[1] Helmholtz AAI 
    https://aai.helmholtz.de
[2] REFEDS Assurance Framework (RAF) 
    https://refeds.org/assurance
[3] Authentication and Authorisation for Recearch Communities (AARC)
    https://aarc-community.org/

